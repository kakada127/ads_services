<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('web');;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// OAuth Routes
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::group(['prefix' => 'home',  'middleware' => 'auth'], function()
{
   // user	
   Route::get('profile', 'User\ProfileController@profile')->name('profile');
   Route::post('updateprofile', 'User\ProfileController@updateProfile')->name('updateProfile');
   // app_profile
   Route::get('profile/view', 'App\AppProfileController@view_app_profile')->name('profile.view');
   Route::post('profile/update', 'App\AppProfileController@update_app_profile')->name('profile.update');
   //Menu

   Route::get('Menu', 'Menu\MenuController@menu')->name('menu');
   Route::post('Menu/add', 'Menu\MenuController@add_menu')->name('add_menu');
   Route::get('Menu/delete/{id}', 'Menu\MenuController@delete_menu')->name('delete_menu');
   Route::get('Menu/update/{id}', 'Menu\MenuController@update_menu')->name('update_menu');
   Route::post('Menu/saveupdate/{id}', 'Menu\MenuController@saveupdate_menu')->name('save.update_menu');


});