<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\App_profile;

class AppProfileController extends Controller
{
    //
    public function view_app_profile(){
    	$app_profile = App_profile::all()->first();
    	return view('app_profile')->with('app_profile', $app_profile);
    }

    public function update_app_profile(Request $request){
    	$app = App_profile::find(1);
    	$image  = $request->logo;
        $newimg = "";
        if($image !=""){
            $newimg = time().''.$image->getClientOriginalName();
            $image->move('photo/logo', $newimg);    	
            $app->logo = $newimg;           
        }
    	$app->profile_name = $request->name;

    	$app->address = $request->address;
    	$app->phone = $request->phone;
    	$app->email = $request->email;
    	$app->twitter = $request->twitter;
    	$app->facebook = $request->facebook;
    	$app->youtube = $request->youtube;
    	$app->linkedin = $request->linkedin;
    	$app->description = $request->description;
    	$app->more = $request->more;
    	$app->save();
    	return redirect()->route('profile.view');
    }

}
