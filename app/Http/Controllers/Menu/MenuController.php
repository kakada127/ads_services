<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
class MenuController extends Controller
{
    //
	public function menu(){
		$menu = Menu::get();
		return view('menu.add_menu')->with('menu', $menu);
			}

	public function add_menu(Request $req){
		$this->validate($req, [
			'menu_name' => 'required',
			'menu_name_kh' => 'required'
		]);
		Menu::create($req->all());
		return redirect()->route('menu');
		echo "hello";
	}
	public function delete_menu($id){
        $del=Menu::where('id',$id)->delete();
        return redirect()->route('menu');
	}

	public function update_menu($id){
		$listmenu = Menu::get();
		$menu = Menu::where('id', $id)->get()->first();
		return view('menu.update_menu', [
			'menu' => $listmenu,
			'menus' => $menu
		]);
	}
	public function saveupdate_menu(Request $req, $id){
		$this->validate($req, [
			'menu_name' => 'required',
			'menu_name_kh' => 'required'
		]);
		Menu::where('id', '=', $id)->update([
			'menu_name' => $req->menu_name,
			'menu_name_kh' => $req->menu_name_kh,
			'parent_id' => $req->parent_id,
			'location_id' => $req->location_id,
			'menu_note' => $req->menu_note,
			'menu_type' => $req->menu_type
		]);	
		return redirect()->route('menu');
	}
}
