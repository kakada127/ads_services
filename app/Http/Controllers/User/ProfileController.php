<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
class ProfileController extends Controller
{
    //Edit Profile

    public function profile(){
    	return view('user.profile');
    }

    public function updateProfile(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
		$user = Auth::user();
		$image  = $request->avatar;
        $newimg = "";
        if($image !=""){
            $newimg = time().''.$image->getClientOriginalName();
            $image->move('photo/user', $newimg); 
            $user->avatar = $newimg;          
        }
		$user->name  = $request->name;
		$user->email = $request->email;		
		$user->phone = $request->phone;
		$user->sex   = $request->sex;
		$user->address = $request->address;
		$user->save();
		return redirect()->route('profile');
    }

}
