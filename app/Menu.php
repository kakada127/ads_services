<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $fillable = ['menu_name', 'menu_name_kh', 'parent_id', 'menu_type', 'note', 'location_id'];
}
