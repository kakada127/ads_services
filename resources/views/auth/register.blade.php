@extends('layouts.app')

@section('title')

	Register

@endsection

@section('content')

		<div id="page-wrapper">
			<div class="main-page login-page ">
				<h2 class="title1">Register</h2>
				<div class="widget-shadow">
					<div class="login-body">
	                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
	                        @csrf
		                        <input id="name" type="text" class="name{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Enter Name">

		                        @if ($errors->has('name'))
		                            <span class="invalid-feedback" role="alert">
		                                <strong>{{ $errors->first('name') }}</strong>
		                            </span>
		                        @endif
	                                <input id="email" type="email" class="user{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email Address">

	                                @if ($errors->has('email'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif

	                                <input id="password" type="password" class="lock{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

	                                @if ($errors->has('password'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('password') }}</strong>
	                                    </span>
	                                @endif
	                                <input id="password-confirm" type="password" class="lock" name="password_confirmation" required placeholder="Confirm Password">
	                        		<input type="submit" name="Sign In" value="Sign Up">
									<div class="registration">
										Already have account ?
										<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
									</div>	                        		
	                    </form>						
                	</div>
				</div>
				
			</div>
		</div>

@endsection