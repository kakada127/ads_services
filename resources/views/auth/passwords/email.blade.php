@extends('layouts.app')

@section('content')
<div id="page-wrapper">
    <div class="main-page login-page ">
        <h2 class="title1">{{ __('Reset Password') }}</h2>
        <div class="widget-shadow">
            <div class="login-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif                
                <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                    @csrf
                        <input id="email" type="email" class="user{{ $errors->has('email') ? ' is-invalid' : '' }}" 
                        name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required="">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    <input type="submit" name="Sign In" value="Reset Password">
                    <div class="registration">
                        Don't have an account ?
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Create an Account') }}</a>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>
@endsection
