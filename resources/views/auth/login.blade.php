@extends('layouts.app')

@section('title')
	
	LOGIN

@endsection

@section('content')
		<div id="page-wrapper">
			<div class="main-page login-page ">
				<h2 class="title1">Login</h2>
				<div class="widget-shadow">
					<div class="login-body">
						<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
						@csrf
	                        <input id="email" type="email" class="user{{ $errors->has('email') ? ' is-invalid' : '' }}" 
	                        name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required="">

	                        @if ($errors->has('email'))
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $errors->first('email') }}</strong>
	                            </span>
	                        @endif

                            <input id="password" type="password" class="lock{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required="">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
							<div class="forgot-grid">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Remember me</label>
								<div class="forgot">
                                <a href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
								</div>
								<div class="clearfix"> </div>
							</div>
							<input type="submit" name="Sign In" value="Sign In">
							<div class="registration">
								Don't have an account ?
								<a class="nav-link" href="{{ route('register') }}">{{ __('Create an Account') }}</a>
								<a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
							</div>
						</form>
					</div>
				</div>
				
			</div>
		</div>
@endsection