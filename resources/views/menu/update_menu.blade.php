@extends('main_layout')
@section('title')
Menu
@stop
@section('content')
<div class="forms">
	<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
		<div class="form-title">
			<h4>Menu</h4>
		</div>
		<div class="form-body">
			<div class="row">					
				<form method="POST" action="{{route('save.update_menu', ['id' => $menus->id])}}" enctype="multipart/form-data">
					@csrf						
					<div class="col-md-12">
						<div class="col-md-6">	
							<h4 class="text-header">Update Menu</h4>
							<label>Menu Name</label>
								@if ($errors->has('menu_name'))
			                        <span class="invalid-feedback" role="alert">
			                            <label class="text-danger">{{ $errors->first('menu_name') }}</label>
			                        </span>
			                    @endif
								<input type="text" name="menu_name" class="form-input" placeholder="Enter menu name"
								value="{{ $menus->menu_name }}">
							<label>Menu Name KH</label>
								@if ($errors->has('menu_name_kh'))
			                        <span class="invalid-feedback" role="alert">
			                            <label class="text-danger">{{ $errors->first('menu_name_kh') }}</label>
			                        </span>
			                    @endif
								<input type="text" name="menu_name_kh" class="form-input" placeholder="Enter menu name khmer"
								value="{{ old('menu_name_kh') }}">
							<label>Menu Parent</label>
								<select class="form-input" name="parent_id">
									<option value="0">
										-----Select Menu Parent----
									</option>
								</select>
							<label>Menu Parent</label>
								<select class="form-input" name="menu_type">
									<option value="0">
										-----Select Menu Type----
									</option>
									<option value="blog">
										Blog
									</option>
								</select>
							<label>Menu Location</label>
								<select class="form-input" name="location_id">
									<option value="0">
										-----Select Menu Location----
									</option>
								</select>
							<label>Note</label>
								<textarea name="menu_note" class="textarea-input" rows="7"></textarea>				
	                		<input type="submit" name="Sign In" value="Save" class="update_btn">					
						</div>
						<div class="col-md-6">
							<h4 class="text-header">List Menu</h4>	
								<table class="table table-bordered">
								 <thead class="table-data">
								  <tr>
								  	<th>No</th>
								  	<th>Menu Name</th> 
								  	<th>Menu Name Kh</th> 
								  	<th>Parent</th> 
								  	<th>Location</th> 
								  	<th>Menu Type</th> 
								  	<th>Status</th>
								  	<th>Action</th>
								  	</tr>
								  </thead>
								  	 <tbody class="table-content">
								  	 	@foreach($menu as $menu)
								  	  <tr> 
								  	  	<th scope="row">{{ $loop->iteration }}</th>
								  	  	 <td>{{$menu->menu_name}}</td>
								  	  	 <td>{{$menu->menu_name_kh}}</td>
								  	  	 <td>{{$menu->parent_id}}</td>
								  	  	 <td>{{$menu->location_id}}</td>	
								  	  	 <td>{{$menu->menu_type}}</td>	
								  	  	 <td>
								  	  	 	@if($menu->menu_status == 1)
								  	  	 	{{'Active'}}
								  	  	 	@else
								  	  	 	{{'Inactive'}}
								  	  	 	@endif
								  	  	 </td>
								  	  	 <td>
								  	  	 	<a href="{{route('delete_menu', ['id' => $menu->id])}}"
								  	  	 	onclick="return confirm('Are you sure you want to delete this?')"
								  	  	 	 class="action delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
								  	  	 	<a href="{{route('update_menu', ['id' => $menu->id])}}"
								  	  	 	 class="action"><i class="fa fa-edit" aria-hidden="true"></i></a>
								  	  	 </td>					  	  
								  	  </tr>
								  	  	@endforeach
								  	  </tbody> 
								  </table> 																 
	                	</div>
	               	</div>	                        
                </form>
			</div>
		</div>
	</div>
</div>
@stop