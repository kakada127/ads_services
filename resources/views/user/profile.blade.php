@extends('main_layout')
@section('title')
Profile
@endsection
@section('content')
<div class="forms">
	<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
		<div class="form-title">
			<h4>Profiles</h4>
		</div>
		<div class="form-body">
			<div class="row">						
				<form method="POST" action="{{ route('updateProfile') }}" enctype="multipart/form-data">
					@csrf						
					<div class="col-md-12">
					<div class="col-md-3">
						<img src="{{asset('photo/user/')}}/{{Auth::user()->avatar}}" class="img-profile img-circle" width="250px" height="250px">
						<input type="file" name="avatar" class="avatar">							
					</div>
					<div class="col-md-8">											 
						<label>Name</label>
							@if ($errors->has('name'))
		                        <span class="invalid-feedback" role="alert">
		                            <label class="text-danger">{{ $errors->first('name') }}</label>
		                        </span>
		                    @endif
						<input type="text" name="name" class="form-input" placeholder="Enter Name"
						value="{{Auth::user()->name}}">
						<label>Email</label>
							@if ($errors->has('email'))
		                        <span class="invalid-feedback" role="alert">
		                            <label class="text-danger">{{ $errors->first('email') }}</label>
		                        </span>
		                    @endif
						<input type="text" name="email" class="form-input" placeholder="Enter Email Address"
						value="{{Auth::user()->email}}">
						<label>Sex</label>
						<select class="form-input" name="sex">
							<option value="Male">
								Male
							</option>
							<option value="Female">
								Female
							</option>
						</select>
						<label>Address</label>
						<input type="text" name="address" class="form-input" placeholder="Enter Address"
						value="{{Auth::user()->address}}">							
						<label>Phone</label>
						<input type="text" name="phone" class="form-input" placeholder="Enter Phone number"
						value="{{Auth::user()->phone}}">
		                <input type="submit" name="Sign In" value="Update" class="update_btn">		
	                </div>
	                </div>                        
                </form>
			</div>
		</div>
	</div>
</div>
@endsection