@extends('main_layout')
@section('title')
Profile
@endsection
@section('content')
<div class="forms">
	<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
		<div class="form-title">
			<h4>App Profiles</h4>
		</div>
		<div class="form-body">
				<div class="row">						
					<form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
						@csrf						
						<div class="col-md-12">
						<div class="col-md-3">
							<img src="{{asset('photo/logo/')}}/{{$app_profile->logo}}" class="img-circle img-profile" width="250px" height="250px">
							<input type="file" name="logo" class="avatar">							
						</div>
						<div class="col-md-8">											 
								<label>App Name</label>
								<input type="text" name="name" class="form-input" placeholder="Enter Name"
								value="{{$app_profile->profile_name}}">

								<label>Address</label>
								<input type="text" name="address" class="form-input" placeholder="Enter Address"
								value="{{$app_profile->address}}">

								<label>Phone</label>
								<input type="text" name="phone" class="form-input" placeholder="Enter Phone"
								value="{{$app_profile->phone}}">

								<label>Email</label>
								<input type="text" name="email" class="form-input" placeholder="Enter Email"
								value="{{$app_profile->email}}">

								<label>Facebook</label>
								<input type="text" name="facebook" class="form-input" placeholder="Enter Facebook"
								value="{{$app_profile->facebook}}">

								<label>YouTube</label>
								<input type="text" name="youtube" class="form-input" placeholder="Enter YouTube"
								value="{{$app_profile->youtube}}">

								<label>Twitter</label>
								<input type="text" name="twitter" class="form-input" placeholder="Enter Twitter"
								value="{{$app_profile->twitter}}">

								<label>Linkedin</label>
								<input type="text" name="linkedin" class="form-input" placeholder="Enter Linkedin"
								value="{{$app_profile->linkedin}}">

								<label>Description</label>
								<input type="text" name="description" class="form-input" placeholder="Enter Description"
								value="{{$app_profile->description}}">

								<label>More</label>
								<input type="text" name="more" class="form-input" placeholder="Enter More"
								value="{{$app_profile->more}}">
			                        <input type="submit" name="Sign In" value="Update" class="update_btn">		                        
		                    </form>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
@endsection