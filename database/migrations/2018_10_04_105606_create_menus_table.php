<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('parent_id')->length(10)->unsigned();
            $table->string('menu_name');
            $table->string('menu_name_kh')->nullable();
            $table->string('menu_type');
            $table->integer('location_id')->length(10)->unsigned();
            $table->string('menu_note')->nullable();
            $table->tinyInteger('menu_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
